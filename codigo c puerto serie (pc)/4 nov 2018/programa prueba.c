#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include "serial.h"
#include "serial.c"

#define timeout 1000000
#define long_info 8

int main()
{
	int serial_fd;
	char puerto[13];
	char info[8];
	
	
	
	while(1){
	printf("Ingrese la direccion del puerto a usar:\n");
	gets(puerto);
	
	printf("El puerto a usar es: %s\n", puerto);
	
	serial_fd=serial_open(puerto,B9600);
	if(serial_fd==-1){
		printf("Error al intentar abrir el puerto %s.",puerto);
		perror("OPEN");
		exit(0);
		}
	printf("ingrese la informacion\n");
	gets(info);
	printf("long_info: %d \n", long_info);
	
	serial_send(serial_fd,info,long_info);
	
	int n;
	n=serial_read(serial_fd,info,long_info,timeout);
	
	if(n>0){
		printf("La cadena es: %s\n", info);
		}
	else{
		printf("Timeout!\n");
		}
		
	serial_close(serial_fd);	
	
	
	
	}
	return 0;
}

