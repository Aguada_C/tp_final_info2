#include <stdio.h>
#include <termios.h>
#include <stdlib.h>
#include <string.h>
#include "serial.h"
#include "serial.c"

#define timeout 1000000
#define long_info 8

int main()
{
	int serial_fd;
	char puerto[13];
	char info[8];
	
	printf("Ingrese la direccion del puerto a usar:\n");
	gets(puerto);
	
	printf("El puerto a usar es: %s\n", puerto);
	
	serial_fd=serial_open(puerto,B9600);
	if(serial_fd==-1){
		printf("Error al intentar abrir el puerto %s.",puerto);
		perror("OPEN");
		exit(0);
		}
	
	while(1){
		printf("Seleccione la opcion de la informacion que desea recibir:\n");
		printf("1- tiempo en seg.\n");
		printf("2- tiempo en miliseg.\n");
		printf("3- velocidad en m/seg.\n");
		printf("4- velocidad en km/hr.\n");
	// 00 (0) tiempo segundo
	// 01 (1) tiempo mili
	// 10 (2) velocidad m/s
	// 11 (3) velocidad km/h
	
	char i;
	int n=0;
	
	i= getchar();
	switch(i) 
	{

    case  '1' : 
      printf("Ha seleccionado tiempo en segundos\n");
	  strcpy(info, "00");	
	  serial_send(serial_fd,info,long_info);	
	  n=serial_read(serial_fd,info,long_info,timeout);	
	  if(n>0){
		printf("El tiempo es: %s segundos\n", info);
		}
	  else{
		printf("Timeout!\n");
		}
		
	serial_close(serial_fd);	
      break;
	
    case '2'  :
      printf("Ha seleccionado tiempo en milisegundos\n");
	  strcpy(info, "01");	
	  serial_send(serial_fd,info,long_info);	
	  n=serial_read(serial_fd,info,long_info,timeout);	
	  if(n>0){
		printf("El tiempo es: %s milisegundos\n", info);
		}
	  else{
		printf("Timeout!\n");
		}
		
	serial_close(serial_fd);
      break;
      
     case '3'  :
      printf("Ha seleccionado velocidad en m/seg.\n");
	  strcpy(info, "10");	
	  serial_send(serial_fd,info,long_info);	
	  n=serial_read(serial_fd,info,long_info,timeout);	
	  if(n>0){
		printf("La velocidad es: %s m/seg.\n", info);
		}
	  else{
		printf("Timeout!\n");
		}
		
	serial_close(serial_fd);	
      break;
      
     case '4' :
      printf("Ha seleccionado velocidad en km/hr\n");
	  strcpy(info, "11");	
	  serial_send(serial_fd,info,long_info);	
	  n=serial_read(serial_fd,info,long_info,timeout);	
	  if(n>0){
		printf("ELa velocidad es: %s km/hr\n", info);
		}
	  else{
		printf("Timeout!\n");
		}
		
	serial_close(serial_fd);	
      break; 

     default :
     printf("ERROR EN LA MATRIX\n");
     break;
}	
	
	}
	return 0;
}

